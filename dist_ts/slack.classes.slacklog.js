"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackLog = void 0;
const slack_classes_slackmessage_1 = require("./slack.classes.slackmessage");
class SlackLog {
    constructor(optionsArg) {
        this.completeLog = ``;
        this.slackAccount = optionsArg.slackAccount;
        this.channelName = optionsArg.channelName;
    }
    async sendLogLine(logText) {
        if (!this.slackMessage) {
            this.slackMessage = new slack_classes_slackmessage_1.SlackMessage(this.slackAccount, {
                text: '``` log is loading... ```'
            });
            await this.slackMessage.sendToRoom(this.channelName);
        }
        const date = new Date();
        this.completeLog +=
            `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} - ` + logText + '\n';
        await this.slackMessage.updateAndSend({
            text: '```\n' + this.completeLog + '\n```'
        });
    }
}
exports.SlackLog = SlackLog;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xhY2suY2xhc3Nlcy5zbGFja2xvZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3RzL3NsYWNrLmNsYXNzZXMuc2xhY2tsb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsNkVBQTREO0FBRTVELE1BQWEsUUFBUTtJQU9uQixZQUFZLFVBQStEO1FBRnBFLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBR3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUM1QyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7SUFDNUMsQ0FBQztJQUNNLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBZTtRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN0QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUkseUNBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUN0RCxJQUFJLEVBQUUsMkJBQTJCO2FBQ2xDLENBQUMsQ0FBQztZQUNILE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVztZQUNkLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3JGLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7WUFDcEMsSUFBSSxFQUFFLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU87U0FDM0MsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBekJELDRCQXlCQyJ9