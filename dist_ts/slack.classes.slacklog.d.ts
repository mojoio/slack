import { SlackAccount } from './slack.classes.slackaccount';
import { SlackMessage } from './slack.classes.slackmessage';
export declare class SlackLog {
    slackAccount: SlackAccount;
    slackMessage: SlackMessage;
    channelName: string;
    completeLog: string;
    constructor(optionsArg: {
        slackAccount: SlackAccount;
        channelName: string;
    });
    sendLogLine(logText: string): Promise<void>;
}
